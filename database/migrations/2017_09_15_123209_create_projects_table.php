<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Project;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description',1000);
            $table->integer('projectmanager_id')->unsigned();
            $table->integer('leaddeveloper_id')->unsigned();
            $table->integer('uxuidesigner_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->string('status')->default(Project::NON_ACTIVE);
            $table->integer('projectaction_id')->unsigned();
            $table->integer('specification_id')->unsigned();
            $table->timestamps();

            $table->foreign('projectmanager_id')->references('id')->on('users');
            $table->foreign('leaddeveloper_id')->references('id')->on('users');
            $table->foreign('uxuidesigner_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('projectaction_id')->references('id')->on('projectactions');
            $table->foreign('specification_id')->references('id')->on('specifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
