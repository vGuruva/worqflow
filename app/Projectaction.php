<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Action;
use App\Project;

class Projectaction extends Model
{
    use SoftDeletes;

    protected $date = ['deleted_at'];
    
    protected $fillable = [
    	'action_id',
    	'user_id',
    ];

    public function action(){
    	return $this->belongsTo(Action::class);
    }  
}
