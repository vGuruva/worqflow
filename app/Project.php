<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Projectaction;
use App\Specification;
use App\Projectmanager;
use App\Uxuidesigner;
use App\Client;

class Project extends Model
{
     use SoftDeletes;

    protected $date = ['deleted_at'];

	const ACTIVE = 'active';
	const NON_ACTIVE = 'not active';

    protected $fillable = [
    	'name',
    	'description',
    	'projectmanager_id',
        'leaddeveloper_id',
        'uxuidesigner_id',
        'client_id',
    	'status',
    	'projectaction_id',
    	'specification_id',
    ];

    public function isActive(){
    	return $this->status == Project::ACTIVE;
    }

    public function categories(){
    	return $this->belongsToMany(Category::class);
    }

    public function projectaction(){
    	return $this->belongsTo(Projectaction::class);
    }

    public function projectmanager(){
        return $this->belongsTo(Projectmanager::class);
    }

    public function uxuidesigner(){
        return $this->belongsTo(Uxuidesigner::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function specification(){
    	return $this->belongsTo(Specification::class);
    }

}
