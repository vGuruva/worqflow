<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;

class Seller extends User
{
    public function products(){
    	return $this->hasMany(Product::class);
    }
}
