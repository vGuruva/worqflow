<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Project;

class Action extends Model
{
   	use SoftDeletes;

	protected $date = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'description',
    ];

    public function projectactions(){
    	return $this->hasMany(Project::class);
    }
}
