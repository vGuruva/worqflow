<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use App\User;

class Uxuidesigner extends User
{
    public function projects()
    {
    	return $this->hasMany(Project::class);
    }
}
