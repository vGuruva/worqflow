<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Product;

class Category extends Model
{
	use SoftDeletes;

	protected $date = ['deleted_at'];

	protected $hidden = [
		'pivot'
	];

    protected $fillable = [
    	'name',
    	'description',
    ];

    public function products(){
    	return $this->belongsToMany(Product::class);
    }
}
