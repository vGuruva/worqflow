<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Project;

class Specification extends Model
{
    use SoftDeletes;

    protected $date = ['deleted_at'];

    protected $fillable = [
    	'name',
    	'description',
    ];

    public function transactions(){
    	return $this->hasMany(Project::class);
    }
}
