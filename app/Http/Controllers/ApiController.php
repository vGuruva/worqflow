<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponder;

class ApiController extends Controller
{
     use ApiResponder;
}
