<?php

namespace App\Http\Controllers\Accountmanager;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Accountmanager;

class AccountmanagerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Accountmanager::has('projects')->get();
        
        return $this->showAll($projects);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Accountmanager $accountmanager)
    {
        return $this->showOne($accountmanager);
    }

}
