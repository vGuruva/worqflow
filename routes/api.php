<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('buyers', 'Buyer\BuyerController', ['only'=>['index', 'show']]);

Route::resource('accountmanagers', 'Accountmanager\AccountmanagerController', ['only'=>['index', 'show']]);

Route::resource('projectmanager', 'Projectmanager\ProjectmanagerController', ['only'=>['index', 'show']]);

Route::resource('leaddeveloper', 'Leaddeveloper\LeaddeveloperController', ['only'=>['index', 'show']]);

Route::resource('uxuidesigner', 'Uxuidesigner\UxuidesignerController', ['only'=>['index', 'show']]);

Route::resource('buyers.transactions', 'Buyer\BuyerTransactionController', ['only'=>['index']]);

Route::resource('buyers.products', 'Buyer\BuyerProductController', ['only'=>['index']]);

Route::resource('buyers.seller', 'Buyer\BuyerSellerController', ['only'=>['index']]);

Route::resource('buyers.categories', 'Buyer\BuyerCategoryController', ['only'=>['index']]);

Route::resource('categories', 'Category\CategoryController', ['except'=>['create', 'edit']]);

Route::resource('categories.products', 'Category\CategoryProductController', ['only'=>['index']]);

Route::resource('categories.seller', 'Category\CategorySellerController', ['only'=>['index']]);

Route::resource('categories.transactions', 'Category\CategoryTransactionController', ['only'=>['index']]);

Route::resource('categories.buyers', 'Category\CategoryBuyerController', ['only'=>['index']]);

Route::resource('products', 'Product\ProductController', ['only'=>['index', 'show']]);

Route::resource('sellers', 'Seller\SellerController', ['only'=>['index', 'show']]);

Route::resource('sellers.transactions', 'Seller\SellerTransactionController', ['only'=>['index']]);

Route::resource('sellers.categories', 'Seller\SellerCategoryController', ['only'=>['index']]);

Route::resource('sellers.products', 'Seller\SellerProductController', ['except'=>['create','show','edit']]);

Route::resource('sellers.buyers', 'Seller\SellerBuyerController', ['only'=>['index']]);

Route::resource('transactions', 'Transaction\TransactionController', ['only'=>['index', 'show']]);

Route::resource('transactions.categories', 'Transaction\TransactionCategoryController', ['only'=>['index']]);

Route::resource('transactions.seller', 'Transaction\TransactionSellerController', ['only'=>['index']]);

Route::resource('transactions.buyers', 'Transaction\TransactionBuyerController', ['only'=>['index']]);

Route::resource('transactions.products', 'Transaction\TransactionProductController', ['only'=>['index']]);

Route::resource('users', 'User\UserController', ['except'=>['create', 'edit']]);

